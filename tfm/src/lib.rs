use byteorder::{BigEndian, ReadBytesExt};
use fixed::{
    types::extra::U20,
    FixedI32,
};
use std::io::Read;

#[derive(Debug)]
pub enum Error {
    Read(std::io::Error),
    Invalid,
}

pub type Result<T> = std::result::Result<T, Error>;

pub struct Preamble {
    pub lf: u16, // length of file, in words
    pub lh: u16, // length of header data, in words
    pub bc: u16, // smallest character code in the font
    pub ec: u16, // largest character code in the font
    pub nw: u16, // number of words in the width table
    pub nh: u16, // number of words in the height table
    pub nd: u16, // number of words in the depth table
    pub ni: u16, // number of words in the italic correction table
    pub nl: u16, // number of words in the lig/kern table
    pub nk: u16, // number of words in the kern table
    pub ne: u16, // number of words in the extensible character table
    pub np: u16, // number of font parameter words
}

fn read_u16(reader: &mut impl Read) -> Result<u16> {
    reader.read_u16::<BigEndian>().map_err(Error::Read)
}

fn read_word(reader: &mut impl Read) -> Result<[u8; 4]> {
    let mut word: [u8; 4] = [0, 0, 0, 0];
    reader.read_exact(&mut word).map_err(Error::Read)?;
    Ok(word)
}

fn read_words(reader: &mut impl Read, count: u16) -> Result<Vec<[u8; 4]>> {
    let mut words: Vec<[u8; 4]> = vec![];
    for _ in 0..count {
        words.push(read_word(reader)?);
    }
    Ok(words)
}

impl Preamble {
    pub fn from_reader(reader: &mut impl Read) -> Result<Preamble> {
        let lf = read_u16(reader)?;
        let lh = read_u16(reader)?;
        let bc = read_u16(reader)?;
        let ec = read_u16(reader)?;
        let nw = read_u16(reader)?;
        let nh = read_u16(reader)?;
        let nd = read_u16(reader)?;
        let ni = read_u16(reader)?;
        let nl = read_u16(reader)?;
        let nk = read_u16(reader)?;
        let ne = read_u16(reader)?;
        let np = read_u16(reader)?;
        // Check that
        // bc-1 <= ec <= 255 and ne <= 256
        if ((bc - 1) > ec) | (ec > 255) | (ne > 256) {
            return Err(Error::Invalid);
        }
        // as well as
        // lf = 6 + lh + (ec − bc + 1) + nw + nh + nd + ni + nl + nk + ne + np .
        if lf != (6 + lh + (ec - bc + 1) + nw + nh + nd + ni + nl + nk + ne + np) {
            return Err(Error::Invalid);
        }
        Ok(Preamble {
            lf,
            lh,
            bc,
            ec,
            nw,
            nh,
            nd,
            ni,
            nl,
            nk,
            ne,
            np,
        })
    }
}

// Fixed-point number used in TFM
pub type FixWord = FixedI32<U20>;

pub struct Header {
    // Word 0
    pub checksum: u32,
    // Word 1
    pub design_size: FixWord,
    // Words 2..11
    pub character_encoding: Option<Vec<u8>>,
    // Words 12..16
    pub font_family: Option<Vec<u8>>,
    // Word 17
    pub seven_bit_safe: Option<u8>,
    // two ignored bytes
    pub face: Option<u8>,
    // Word 18.. are also ignored
}

fn words_to_chars(value: &[[u8; 4]]) -> Vec<u8> {
    let mut chars = vec![];
    for word in value {
        for byte in word {
            chars.push(*byte);
        }
    }
    chars
}

impl From<Vec<[u8; 4]>> for Header {
    fn from(value: Vec<[u8; 4]>) -> Self {
        let n_words = value.len();
        let checksum = u32::from_be_bytes(value[0]);
        let design_size = FixWord::from_be_bytes(value[1]);
        let mut character_encoding = None;
        let mut font_family = None;
        let mut seven_bit_safe = None;
        let mut face = None;
        if n_words > 2 {
            let end = std::cmp::min(11, n_words);
            character_encoding = Some(words_to_chars(&value[2..end]));
        }
        if n_words > 12 {
            let end = std::cmp::min(16, n_words);
            font_family = Some(words_to_chars(&value[12..end]));
        }
        if n_words > 17 {
            let word = value[17];
            seven_bit_safe = Some(word[0]);
            face = Some(word[1]);
        }
        Header {
            checksum,
            design_size,
            character_encoding,
            font_family,
            seven_bit_safe,
            face,
        }
    }
}

pub enum Tag {
    No,
    Lig,
    List,
    Ext,
}

impl From<u8> for Tag {
    fn from(value: u8) -> Self {
        let first = (value & 0b10) > 0;
        let second = (value & 0b01) > 0;
        match (first, second) {
            (true, true) => Self::Ext,
            (true, false) => Self::List,
            (false, true) => Self::Lig,
            (false, false) => Self::No,
        }
    }
}

// One word
pub struct CharInfo {
    pub width_index: u8,  // 8 bits
    pub height_index: u8, // 4 bits
    pub depth_index: u8,  // 4 bits
    pub italic_index: u8, // 6 bits
    pub tag: Tag,         // 2 bits
    pub remainder: u8,    // 8 bits
}

impl From<[u8; 4]> for CharInfo {
    fn from(value: [u8; 4]) -> Self {
        CharInfo {
            width_index: value[0],
            height_index: (value[1] >> 4) & 0b00001111,
            depth_index: value[1] & 0b00001111,
            italic_index: (value[2] >> 2) & 0b00111111,
            tag: Tag::from(value[2] & 0b00000011),
            remainder: value[3],
        }
    }
}

// One word
pub struct LigKern {
    pub skip_byte: u8,
    pub next_char: u8,
    pub op_byte: u8,
    pub remainder: u8,
}

impl From<[u8; 4]> for LigKern {
    fn from(value: [u8; 4]) -> Self {
        LigKern {
            skip_byte: value[0],
            next_char: value[1],
            op_byte: value[2],
            remainder: value[3],
        }
    }
}

pub struct ExtensibleRecipe {
    pub top: u8,
    pub mid: u8,
    pub bot: u8,
    pub rep: u8,
}

impl From<[u8; 4]> for ExtensibleRecipe {
    fn from(value: [u8; 4]) -> Self {
        ExtensibleRecipe {
            top: value[0],
            mid: value[1],
            bot: value[2],
            rep: value[3],
        }
    }
}

pub struct Param {
    pub slant: FixWord,
    pub space: FixWord,
    pub space_stretch: FixWord,
    pub space_shrink: FixWord,
    pub x_height: FixWord,
    pub quad: FixWord,
    pub extra_space: FixWord,
}

impl From<Vec<[u8; 4]>> for Param {
    fn from(value: Vec<[u8; 4]>) -> Self {
        // TODO: check that we are in bounds?
        Param {
            slant: FixWord::from_be_bytes(value[0]),
            space: FixWord::from_be_bytes(value[1]),
            space_stretch: FixWord::from_be_bytes(value[2]),
            space_shrink: FixWord::from_be_bytes(value[3]),
            x_height: FixWord::from_be_bytes(value[4]),
            quad: FixWord::from_be_bytes(value[5]),
            extra_space: FixWord::from_be_bytes(value[6]),
        }
    }
}

pub struct TFM {
    pub header: Header,               // 0..lh-1
    pub char_info: Vec<CharInfo>,     // bc..ec
    pub width: Vec<FixWord>,          // 0..nw-1
    pub height: Vec<FixWord>,         // 0..nh-1
    pub depth: Vec<FixWord>,          // 0..nd-1
    pub italic: Vec<FixWord>,         // 0..ni-1
    pub lig_kern: Vec<LigKern>,       // 0..nl-1
    pub kern: Vec<FixWord>,           // 0..nk-1
    pub exten: Vec<ExtensibleRecipe>, // 0..ne-1
    pub param: Param,            // 1..np
}

impl TFM {
    pub fn from_reader(reader: &mut impl Read) -> Result<Self> {
        let preamble = Preamble::from_reader(reader)?;
        let header = Header::from(read_words(reader, preamble.lh)?);
        let char_info = read_words(reader, preamble.bc - preamble.ec)?
            .into_iter()
            .map(CharInfo::from)
            .collect();
        let width = read_words(reader, preamble.nw)?
            .into_iter()
            .map(FixWord::from_be_bytes)
            .collect();
        let height = read_words(reader, preamble.nh)?
            .into_iter()
            .map(FixWord::from_be_bytes)
            .collect();
        let depth = read_words(reader, preamble.nd)?
            .into_iter()
            .map(FixWord::from_be_bytes)
            .collect();
        let italic = read_words(reader, preamble.ni)?
            .into_iter()
            .map(FixWord::from_be_bytes)
            .collect();
        let lig_kern = read_words(reader, preamble.nl)?
            .into_iter()
            .map(LigKern::from)
            .collect();
        let kern = read_words(reader, preamble.nk)?
            .into_iter()
            .map(FixWord::from_be_bytes)
            .collect();
        let exten = read_words(reader, preamble.ne)?
            .into_iter()
            .map(ExtensibleRecipe::from)
            .collect();
        let param = Param::from(read_words(reader, preamble.np)?);
        let tfm = TFM {
            header,
            char_info,
            width,
            height,
            depth,
            italic,
            lig_kern,
            kern,
            exten,
            param,
        };
        Ok(tfm)
    }
}
