\newcount\chapno
\font\chapfont=cmb10 scaled 2000
\def\chapter#1{%
    \begingroup
    \parindent=0pt
    \chapfont
    Chapter \the\chapno:
    #1
    \endgroup
    \advance\chapno by 1
    \vskip2em
}

\chapno=2
\chapter{Well-Being for All}

Well-being for all is not a dream. It is possible, realizable, owing to all
that our ancestors have done to increase our powers of production.

We know, indeed, that the producers, although they constitute hardly one-third
of the inhabitants of civilized countries, even now produce such quantities of
goods that a certain degree of comfort could be brought to every hearth. We
know further that if all those who squander to-day the fruits of others’ toil
were forced to employ their leisure in useful work, our wealth would increase
in proportion to the number of producers, and more. Finally, we know that
contrary to the theory enunciated by Malthus — that Oracle of middle-class
Economics — the productive powers of the human race increase at a much more
rapid ratio than its powers of reproduction. The more thickly men are crowded
on the soil, the more rapid is the growth of their wealth-creating power.

Thus, although the population of England has only increased from 1844 to 1890
by 62 per cent, its production has grown, to say the least, at double that rate
— to wit, by 130 per cent. In France, where the population has grown more
slowly, the increase in production is nevertheless very rapid. Notwithstanding
the crises through which agriculture is frequently passing, notwithstanding
State interference, the blood-tax (conscription), and speculative commerce and
finance, the production of wheat in France has increased fourfold, and
industrial production more than tenfold, in the course of the last eighty
years. In the United States the progress is still more striking. In spite of
immigration, or rather precisely because of the influx of surplus European
labour, the United States have multiplied their wealth tenfold.

However, these figures give yet a very faint idea of what our wealth might
become under better conditions. For alongside of the rapid development of our
wealth-producing powers we have an overwhelming increase in the ranks of the
idlers and middlemen. Instead of capital gradually concentrating itself in a
few hands, so that it would only be necessary for the community to dispossess a
few millionaires and enter upon its lawful heritage — instead of this Socialist
forecast proving true, the exact reverse is coming to pass: the swarm of
parasites is ever increasing.

In France there are not ten actual producers to every thirty inhabitants. The
whole agricultural wealth of the country is the work of less than seven
millions of men, and in the two great industries, mining and the textile trade,
you will find that the workers number less than two and one-half millions. But
the exploiters of labour, how many are they? — In England (exclusive of
Scotland and Ireland), only one million workers — men, women, and children —
are employed in all the textile trades, rather more than half a million work
the mines, rather less than half a million till the ground, and the
statisticians have to exaggerate all the figures in order to establish a
maximum of eight million producers to twenty-six million inhabitants. Strictly
speaking the creators of the goods exported from Britain to all the ends of the
earth comprise only from six to seven million workers. And what is the sum of
the shareholders and middlemen who levy the first fruits of labour from far and
near, and heap up unearned gains by thrusting themselves between the producer
and the consumer, paying the former not a fifth, nay, not a twentieth, of the
price they exact from the latter?

Nor is this all. Those who withhold capital constantly reduce the output by
restraining production. We need not speak of the cartloads of oysters thrown
into the sea to prevent a dainty, hitherto reserved for the rich, from becoming
a food for the people. We need not speak of the thousand and one luxuries —
stuffs, foods, etc. etc. — treated after the same fashion as the oysters. It is
enough to remember the way in which the production of the most necessary things
is limited. Legions of miners are ready and willing to dig out coal every day,
and send it to those who are shivering with cold; but too often a third, or
even two-thirds, of their number are forbidden to work more than three days a
week, because, forsooth, the price of coal must be kept up? Thousands of
weavers are forbidden to work the looms, though their wives and children go in
rags, and though three-quarters of the population of Europe have no clothing
worthy the name.

Hundreds of blast-furnaces, thousands of factories periodically stand idle,
others only work half-time — and in every civilized nation there is a permanent
population of about two million individuals who ask only for work, but to whom
work is denied.

How gladly would these millions of men set to work to reclaim waste lands, or
to transform illcultivated land into fertile fields, rich in harvests! A year
of well-directed toil would suffice to multiply fivefold the produce of dry
lands in the south of France which now yield only about eight bushels of wheat
per acre. But these men, who would be happy to become hardy pioneers in so many
branches of wealth-producing activity, must stay their hands because the owners
of the soil, the mines, and the factories prefer to invest their capital —
stolen in the first place from the community — in Turkish or Egyptian bonds, or
in Patagonian gold mines, and so make Egyptian fellahs, Italian exiles, and
Chinese coolies their wage-slaves.

So much for the direct and deliberate limitation of production; but there is
also a limitation indirect and not of set purpose, which consists in spending
human toil on objects absolutely useless, or destined only to satisfy the dull
vanity of the rich.

It is impossible to reckon in figures the extent to which wealth is restricted
indirectly, the extent to which energy is squandered, that might have served to
produce, and above all to prepare the machinery necessary to production. It is
enough to cite the immense sums spent by Europe in armaments for the sole
purpose of acquiring control of the markets, and so forcing her own commercial
standards on neighbouring territories and making exploitation easier at home;
the millions paid every year to officials of all sorts, whose function it is to
maintain the rights of minorities — the right, that is, of a few rich men — to
manipulate the economic activities of the nation; the millions spent on judges,
prisons, policemen, and all the paraphernalia of so-called justice — spent to
no purpose, because we know that every alleviation, however slight, of the
wretchedness of our great cities is followed by a very considerable diminution
of crime; lastly, the millions spent on propagating pernicious doctrines by
means of the press, and news “cooked” in the interest of this or that party, of
this politician or of that company of exploiters.

But over and above this we must take into account all the labour that goes to
sheer waste, in keeping up the stables, the kennels, and the retinue of the
rich, for instance; in pandering to the caprices of society and to the depraved
tastes of the fashionable mob; in forcing the consumer on the one hand to buy
what he does not need, or foisting an inferior article upon him by means of
puffery, and in producing on the other hand wares which are absolutely
injurious, but profitable to the manufacturer. What is squandered in this
manner would be enough to double our real wealth, or so to plenish our mills
and factories with machinery that they would soon flood the shops with all that
is now lacking to two-thirds of the nation. Under our present system a full
quarter of the producers in every nation are forced to be idle for three or
four months in the year, and the labour of another quarter, if not of the half,
has no better results than the amusement of the rich or the exploitation of the
public.

Thus, if we consider on the one hand the rapidity with which civilized nations
augment their powers of production, and on the other hand the limits set to
that production, be it directly or indirectly, by existing conditions, one
cannot but conclude that an economic system a trifle more enlightened would
permit them to heap up in a few years so many useful products that they would
be constrained to cry — “Enough! We have enough coal and bread and raiment !
Let us rest and consider how best to use our powers, how best to employ our
leisure.”

No, plenty for all is not a dream — though it was a dream indeed in those old
days when man, for all his pains, could hardly win a bushel of wheat from an
acre of land, and had to fashion by hand all the implements he used in
agriculture and industry. Now it is no longer a dream, because man has invented
a motor which, with a little iron and a few pounds of coal, gives him the
mastery of a creature strong and docile as a horse, and capable of setting the
most complicated machinery in motion.

But, if plenty for all is to become a reality, this immense capital — cities,
houses, pastures, arable lands, factories, highways, education — must cease to
be regarded as private property, for the monopolist to dispose of at his
pleasure.

This rich endowment, painfully won, builded, fashioned, or invented by our
ancestors, must become common property, so that the collective interests of men
may gain from it the greatest good for all.

There must be EXPROPRIATION. The well-being of all — the end; expropriation —
the means.  

\bye
% vim: ft=plaintex
