use std::fs::File;
use std::io::Read;

use std::{
    fs,
    io::{Seek, SeekFrom},
};

use byteorder::{BigEndian, ReadBytesExt};

type Order = BigEndian;
type Result<T> = std::result::Result<T, std::io::Error>;

#[derive(Debug, Clone)]
pub enum Command {
    Char(u32),
    SetRule(i32, i32),
    Put(u32),
    PutRule(i32, i32),
    Nop,
    Eop,
    Push,
    Pop,
    Bop([i32; 10], i32),
    Right(i32),
    W0,
    W(i32),
    X0,
    X(i32),
    Down(i32),
    Y0,
    Y(i32),
    Z0,
    Z(i32),
    FntNum(u8),
    Fnt(u32),
    Xxx(String), // String may be incorrect here, as it is only recommended to be a string
    FntDef(u32, u32, u32, u32, String),
    Pre(u8, u32, u32, i32, String),
    Post(i32, u32, u32, u32, u32, u32, u16, u16),
    PostPost(u32, u8),
}

fn read_bytes(reader: &mut impl Read, count: usize) -> Result<Vec<u8>> {
    let mut out = vec![0u8; count];
    match reader.read_exact(&mut out) {
        Ok(_) => Ok(out),
        Err(e) => Err(e),
    }
}

fn read_bytes_to_u32(reader: &mut impl Read, count: u8) -> Result<u32> {
    let byte = match count {
        1 => reader.read_u8()? as u32,
        2 => reader.read_u16::<Order>()? as u32,
        3 => reader.read_u24::<Order>()? as u32,
        4 => reader.read_u32::<Order>()? as u32,
        _ => panic!(),
    };
    Ok(byte)
}

fn read_bytes_to_i32(reader: &mut impl Read, count: u8) -> Result<i32> {
    let byte = match count {
        1 => reader.read_i8()? as i32,
        2 => reader.read_i16::<Order>()? as i32,
        3 => reader.read_i24::<Order>()? as i32,
        4 => reader.read_i32::<Order>()? as i32,
        _ => panic!(),
    };
    Ok(byte)
}

impl Command {
    pub fn from_reader(mut reader: &mut impl Read) -> Result<Command> {
        let c = reader.read_u8()?;
        let cmd = match c {
            0..=127 => Command::Char(c as u32), // set_char_0..127
            128..=131 => Command::Char(read_bytes_to_u32(&mut reader, c - 127)?), // set1..4
            132 => {
                let a = reader.read_i32::<Order>()?;
                let b = reader.read_i32::<Order>()?;
                Command::SetRule(a, b) // set_rule
            }
            133..=136 => Command::Put(read_bytes_to_u32(&mut reader, c - 132)?), // put1..4
            137 => {
                let a = reader.read_i32::<Order>()?;
                let b = reader.read_i32::<Order>()?;
                Command::PutRule(a, b) // set_rule
            }
            138 => Command::Nop, // nop
            139 => {
                let mut counter: [i32; 10] = [0; 10];
                for i in 0..counter.len() {
                    counter[i] = reader.read_i32::<Order>()?;
                }
                let p = reader.read_i32::<Order>()?;
                Command::Bop(counter, p)
            } // bop
            140 => Command::Eop, // bop
            141 => Command::Push, // push
            142 => Command::Pop, // push
            143..=146 => Command::Right(read_bytes_to_i32(&mut reader, c - 142)?), // right1..4
            147 => Command::W0,  // w0
            148..=151 => Command::W(read_bytes_to_i32(&mut reader, c - 147)?), // w1..4
            152 => Command::X0,  // x0
            153..=156 => Command::X(read_bytes_to_i32(&mut reader, c - 152)?), // x1..4
            157..=160 => Command::Down(read_bytes_to_i32(&mut reader, c - 156)?), // down1..4
            161 => Command::Y0,  // y0
            162..=165 => Command::Y(read_bytes_to_i32(&mut reader, c - 161)?), // y1..4
            166 => Command::Z0,  // z0
            167..=170 => Command::Z(read_bytes_to_i32(&mut reader, c - 166)?), // z1..4
            171..=234 => Command::FntNum(c - 171),
            235..=238 => Command::Fnt(read_bytes_to_u32(&mut reader, c - 234)?), // fnt1..4
            239..=242 => {
                let k = read_bytes_to_u32(&mut reader, c - 238)?;
                Command::Xxx(String::from_utf8(read_bytes(&mut reader, k as usize)?).unwrap())
            } // Xxx
            243..=246 => {
                let k = read_bytes_to_u32(&mut reader, c - 242)?;
                let c = reader.read_u32::<Order>()?;
                let s = reader.read_u32::<Order>()?;
                let d = reader.read_u32::<Order>()?;
                let a = reader.read_u8()?;
                let l = reader.read_u8()?;
                let n = read_bytes(&mut reader, (a + l) as usize)?;
                Command::FntDef(k, c, s, d, String::from_utf8(n).unwrap())
            } // fnt_def
            247 => {
                let i = reader.read_u8()?;
                let num = reader.read_u32::<Order>()?;
                let den = reader.read_u32::<Order>()?;
                let mag = reader.read_i32::<Order>()?;
                let k = reader.read_u8()?;
                let x = read_bytes(&mut reader, k as usize)?;
                Command::Pre(i, num, den, mag, String::from_utf8(x).unwrap())
            } // pre
            248 => {
                let p = reader.read_i32::<Order>()?;
                let num = reader.read_u32::<Order>()?;
                let den = reader.read_u32::<Order>()?;
                let mag = reader.read_u32::<Order>()?;
                let l = reader.read_u32::<Order>()?;
                let u = reader.read_u32::<Order>()?;
                let s = reader.read_u16::<Order>()?;
                let t = reader.read_u16::<Order>()?;
                Command::Post(p, num, den, mag, l, u, s, t)
            } // post
            249 => {
                let q = reader.read_u32::<Order>()?;
                let i = reader.read_u8()?;
                Command::PostPost(q, i)
            }
            250..=255 => panic!("Unknown command!"),
        };
        Ok(cmd)
    }
}

pub struct Dvi<R: Read> {
    reader: R,
    stopped: bool,
    last_bop: i32,
    last_cmd: u32,
}

impl<R: Read> Dvi<R> {
    pub fn from_reader(reader: R) -> Self {
        Self {
            reader,
            stopped: false,
            last_bop: 0,
            last_cmd: 0,
        }
    }
}

impl<R: Read + Seek> Iterator for Dvi<R> {
    type Item = Command;

    fn next(&mut self) -> Option<Self::Item> {
        if self.stopped {
            return None;
        }
        self.last_cmd = self.stream_position().unwrap() as u32;
        let cmd = Command::from_reader(&mut self.reader).unwrap();
        match cmd {
            Command::PostPost(_, _) => self.stopped = true,
            Command::Bop(_, pos) => self.last_bop = pos as i32,
            Command::Post(pos, _, _, _, _, _, _, _) => self.last_bop = pos,
            _ => {}
        };
        Some(cmd)
    }
}

impl<R: Read + Seek> Seek for Dvi<R> {
    fn seek(&mut self, pos: SeekFrom) -> std::io::Result<u64> {
        self.reader.seek(pos)
    }
}

pub trait Pages {
    fn prev_page(&mut self) -> Option<Command>;
    fn next_page(&mut self) -> Option<Command>;
}

impl<R: Read + Seek> Pages for Dvi<R> {
    fn prev_page(&mut self) -> Option<Command> {
        match self.last_bop {
            std::i32::MIN..=0 => None,
            1..=std::i32::MAX => {
                self.reader
                    .seek(SeekFrom::Start(self.last_bop as u64))
                    .unwrap();
                Some(self.next().unwrap())
            }
        }
    }

    fn next_page(&mut self) -> Option<Command> {
        let mut cmd: Command;
        loop {
            cmd = self.next().unwrap();
            match cmd {
                Command::Bop(_, _) => return Some(cmd),
                Command::Post(_, _, _, _, _, _, _, _) => {
                    self.reader.seek(SeekFrom::Start(self.last_cmd as u64)).unwrap();
                    return None
                },
                Command::PostPost(_, _) => {
                    panic!("post_post encountered while seeking to next page")
                }
                _ => {}
            }
        }
    }
}

#[test]
fn seek_pages() {
    let io = fs::File::open(String::from("samples/sample1.dvi")).unwrap();
    let mut cmds = Dvi::from_reader(io);
    loop {
        let begin = cmds.next_page();
        let pos = cmds.last_cmd;
        match begin {
            Some(cmd) => {
                eprintln!("{pos:04} {cmd:?}");
                assert!(matches!(cmd, Command::Bop(_, _)))
            }, // This is a page start
            None => {
                let next = cmds.next().unwrap();
                eprintln!("{pos:04} {next:?}");
                assert!(matches!(next, Command::Post(_, _, _, _, _, _, _, _)));
                break // encountered the end
            },
        }
    }
    println!("Visited pages forwards");
    loop {
        let begin = cmds.prev_page();
        let pos = cmds.last_cmd;
        match begin {
            Some(cmd) => {
                eprintln!("{pos:04} {cmd:?}");
                assert!(matches!(cmd, Command::Bop(_, _)))
            }, // This is a page start
            None => {
                break; // encountered the beginning
            },
        }
    }
    panic!()
}

#[test]
fn page_refs() {
    let io = fs::File::open(String::from("samples/sample1.dvi")).unwrap();
    let cmds = Dvi::from_reader(io);
    let mut bops = vec![];
    let mut last = Command::Nop;
    for (i, cmd) in cmds.enumerate() {
        //eprintln!("{i:04} {:?}", cmd);
        match cmd {
            Command::Pre(vers, _, _, _, _) => {
                // Check that Pre only shows up in the front
                assert!(i == 0);
                // Check that the version was correctly read
                assert!(vers == 2);
            }
            Command::Bop(num, pos) => {
                // Store bop for later use
                bops.push(Command::Bop(num, pos));
                match pos {
                    // This is the first page and command index 1
                    -1 => assert!(i == 1),
                    // This is the next page and points at the last page
                    _ => assert!(matches!(last, Command::Eop)),
                }
            }
            Command::Post(_, _, _, _, _, _, _, _) => {
                // Store post for later use
                bops.push(cmd.clone());
            }
            _ => {}
        }
        last = cmd;
    }
    // Check that bops are where they should be
    eprintln!("{:?}", bops);
    let mut io = fs::File::open(String::from("samples/sample1.dvi")).unwrap();
    for bop in bops {
        match bop {
            Command::Bop(_, -1) => {}
            Command::Bop(_, pos) => {
                io.seek(SeekFrom::Start(pos as u64)).unwrap();
                let cmd = Command::from_reader(&mut io).unwrap();
                eprintln!("{:?} -> {:?}", bop, cmd);
                assert!(matches!(cmd, Command::Bop(_, _)));
            }
            Command::Post(pos, _, _, _, _, _, _, _) => {
                io.seek(SeekFrom::Start(pos as u64)).unwrap();
                let cmd = Command::from_reader(&mut io).unwrap();
                eprintln!("{:?} -> {:?}", bop, cmd);
                assert!(matches!(cmd, Command::Bop(_, _)));
            }
            _ => {}
        }
    }
}

#[test]
fn read_whole_file() {
    let io = fs::File::open(String::from("samples/sample1.dvi")).unwrap();
    let mut cmds = Dvi::from_reader(io);
    loop {
        match cmds.next() {
            Some(Command::PostPost(_, _)) => {
                assert!(cmds.next().is_none());
            }
            Some(_) => {}
            None => {
                // Test that we only get more none now
                assert!(cmds.stopped == true);
                assert!(cmds.next().is_none());
                break;
            }
        }
    }
}
